## Simple free forms builder

###  There are several modules available that are in the business of allowing to create forms

Looking for free forms?  We have  most features including external form converter, self-totaling fields, calculating fields, conditional fields

#### Our features:

* A/B Testing
* Form Conversion
* Form Optimization
* Branch logic
* Payment integration
* Third party integration
* Push notifications
* Multiple language support
* Conditional logic
* Validation rules
* Server rules
* Custom reports

###   We create an online form instantly

We create [free forms](https://formtitan.com) that is instantly ready to receive information, securely, that can be viewed and/or downloaded at your convenience by our [free form builder](http://www.formlogix.com/)

Happy free forms!